const mongoose = require('mongoose');
const Schma = mongoose.Schema;
require("dotenv").config();

const memSchma = new Schma({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    editedby: {
        type: String,
        required: false
    },
    name: {
        type: String,
        required: false
    },
    lastname: {
        type: String,
        required: false
    },
}, { timestamps: true} );

const mem = mongoose.model(process.env.DB_Employee, memSchma);

module.exports = mem;