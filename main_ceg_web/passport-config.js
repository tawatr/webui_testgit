const mems = require('./models/mem_account');
const LocalStrategy = require("passport-local").Strategy
const bcrypt = require("bcrypt")

module.exports = function(passport) {
    passport.use(
        // new LocalStrategy({usernameField : 'username'},(username,password,done)=> {
            new LocalStrategy((username,password,done)=> {
                //match user
                mems.findOne({username : username})
                .then((user)=>{
                 if(!user) {
                     return done(null,false,{message : 'that username is not registered'});
                 }
                 //match pass
                 bcrypt.compare(password,user.password,(err,isMatch)=>{
                     if(err) throw err;

                     if(isMatch) {
                         console.log(user)
                         return done(null,user);
                     } else {
                         return done(null,false,{message : 'pass incorrect'});
                     }
                 })
                })
                .catch((err)=> {console.log(err)})
        })
    )
    passport.serializeUser(function(user, done) {
        done(null, user.id);
      });
    passport.deserializeUser(function(id, done) {
        mems.findById(id, function(err, user) {
          done(err, user);
        });
      }); 
};
