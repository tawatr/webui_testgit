const mems = require('../models/mem_account');
const bcrypt = require('bcrypt');
const passport = require('passport');
// var client = require('scp2');
var exec = require('ssh-exec');
var formidable = require('formidable');
const { x } = require('pdfkit');
require("dotenv").config();

const inv_model_post = (req,res) => {
    try{
        sess = req.session;
        const m_cmin = req.body.m_cmin;
        if ( m_cmin != undefined ) {
            sess.m_cmin = parseFloat(m_cmin);
        }

        const m_cmax = req.body.m_cmax;
        if ( m_cmax != undefined ) {
            sess.m_cmax = parseFloat(m_cmax);
        }
        
        const m_xmin = req.body.m_xmin;
        if ( m_xmin != undefined ) {
            sess.m_xmin = parseFloat(m_xmin);
        }

        const m_xmax = req.body.m_xmax;
        if ( m_xmax != undefined ) {
            sess.m_xmax = parseFloat(m_xmax);
        }

        const m_zmin = req.body.m_zmin;
        if ( m_zmin != undefined ) {
            sess.m_zmin = parseFloat(m_zmin);
        }

        const m_zmax = req.body.m_zmax;
        if ( m_zmax != undefined ) {
            sess.m_zmax = parseFloat(m_zmax);
        }
        res.redirect('/inv/model');
    } catch {
    }
}

const inv_data_get = (req,res) => {
    sess = req.session;
    sess = validate_option(sess);
    ( async () => {
        let arg = await LoadData(sess.dfile, sess, req.user.name);
        res.render('./inv/data', arg);
    })();
}

const inv_resp_get = (req,res) => {
    sess = req.session;
    sess = validate_option(sess);
    ( async () => {
        let arg = await LoadData(sess.rfile, sess, req.user.name);
        res.render('./inv/resp', arg);
    })();
}

function add_option(sess, arg) {

    if ( arg.d_cmin != undefined && sess.d_cmin == undefined) {
        sess.d_cmin = Math.pow(10,arg.d_cmin);
    }

    if ( arg.d_cmax != undefined && sess.d_cmax == undefined) {
        sess.d_cmax = Math.pow(10,arg.d_cmax);
    }

    if ( arg.pelec != undefined ) {
        if ( sess.m_xmin == undefined) {
            sess.m_xmin = arg.pelec[0][0];
        }

        if ( sess.m_xmax == undefined) {
            sess.m_xmax = arg.pelec[arg.noelec-1][0];
        }

        if ( sess.m_zmax == undefined) {
            sess.m_zmax = (sess.m_xmax - sess.m_xmin)/4;
        }
    }
    
    if ( arg.m_cmin != undefined && sess.m_cmin == undefined) {
        sess.m_cmin = Math.pow(10,arg.m_cmin);
    }

    if ( arg.m_cmax != undefined && sess.m_cmax == undefined) {
        sess.m_cmax = Math.pow(10,arg.m_cmax);
    }

    return sess;
}

function validate_option(sess) {
    if( sess.hasOwnProperty('d_cmin') == false ) {
        sess.d_cmin = undefined;
    }
    
    if( sess.hasOwnProperty('d_cmax') == false ) {
        sess.d_cmax = undefined;
    }

    if( sess.hasOwnProperty('m_cmin') == false ) {
        sess.m_cmin = undefined;
    }

    if( sess.hasOwnProperty('m_cmax') == false ) {
        sess.m_cmax = undefined;
    }

    if( sess.hasOwnProperty('m_xmin') == false ) {
        sess.m_xmin = undefined;
    }

    if( sess.hasOwnProperty('m_xmax') == false ) {
        sess.m_xmax = undefined;
    }

    if( sess.hasOwnProperty('m_zmin') == false ) {
        sess.m_zmin = 0;
    }

    if( sess.hasOwnProperty('m_zmax') == false ) {
        sess.m_zmax = undefined;
    }

    return sess;
}

async function LoadData(dfile, sess, username) {
    let {readData} = require('../functions/readData');
    let {extractData} = require('../functions/extractData');
    let {plotOption_data} = require('../functions/plotOption_data');
    try{
            r = await readData(dfile);
            r = extractData(r);

            rPlot = plotOption_data({r: r,
                cmin: sess.d_cmin, cmax: sess.d_cmax,
            });
                
            sess = add_option(sess, {d_cmin: rPlot.cmin, 
                d_cmax: rPlot.cmax, pelec: r.pelec,
                noelec: r.noelec });

            arg = {title: "welcome page", name: username, 
                apprho: r.apprho, xres: r.xres, zres: r.zres,
                cmin: rPlot.cmin, cmax: rPlot.cmax, clabel: rPlot.clabel,
                xmin: rPlot.xmin, xmax: rPlot.xmax, xlabel: rPlot.xlabel,
                zmin: rPlot.zmin, zmax: rPlot.zmax, zlabel: rPlot.zlabel,
                tick: rPlot.tick, tickval: rPlot.tickval,
                title_plot: rPlot.title};

            return arg;
    } catch(err) {
        console.error(err);
    }
}
async function LoadModel(mfile, sess, username) {
    let {readModel} = require('../functions/readModel');
    let {extractModel} = require('../functions/extractModel');
    let {plotOption_model} = require('../functions/plotOption_model');

    try{
        m = await readModel(mfile);
        m = extractModel(m);
        mPlot = plotOption_model({m: m, 
            cmin: sess.m_cmin, cmax: sess.m_cmax, clabel: 'Resistivity [Ohm m]',
            xmin: sess.m_xmin, xmax: sess.m_xmax, xlabel: 'Surface Distance [m]',
            zmin: sess.m_zmin, zmax: sess.m_zmax, zlabel: 'Elevation [m]', 
            title: 'Inverted Model'
            });
        
        sess = add_option(sess, {m_cmin: mPlot.cmin, 
                m_cmax: mPlot.cmax});
        
        arg = {title: "welcome page", name: username, 
            rho: m.rho, xS: m.xS_plot, zS: m.zS_plot,
            cmin: mPlot.cmin, cmax: mPlot.cmax, clabel: mPlot.clabel,
            xmin: mPlot.xmin, xmax: mPlot.xmax, xlabel: mPlot.xlabel,
            zmin: mPlot.zmin, zmax: mPlot.zmax, zlabel: mPlot.zlabel,
            title_plot: mPlot.title,
            tick: mPlot.tick, tickval: mPlot.tickval}; 
        return arg;
    } catch(err){
        console.error(err);
    }
};

const inv_model_get = (req,res) => {
    sess = req.session;
    sess = validate_option(sess);
    
    ( async () => {
        let arg = await LoadModel(sess.mfile, sess, req.user.name);
        res.render('./inv/model', arg);
    })();
}

const inv_login_post = (req,res,next) => {
    passport.authenticate('local',{
        successRedirect : '/inv/index',
        failureRedirect : '/inv/login',
        failureFlash : true,
        })(req,res,next);
}

const inv_index_post = (req,res) => {
    sess = req.session;
    try{
        var form = new formidable.IncomingForm();
        
        form.parse(req, function(err, fields, files) {
            if ( files.mfile.name.length > 0 ) {
                sess.mfile = files.mfile.path;
            } else {
                sess.mfile = '';
            }

            if ( files.rfile.name.length > 0 ) {
                sess.rfile = files.rfile.path;
            } else {
                sess.rfile = '';
            }
            
            if ( files.dfile.name.length > 0 ) {
                sess.dfile = files.dfile.path;
            } else {
                sess.dfile = '';
            }
            
            
            if ( files.dfile.name.length > 0 ) {
                res.redirect('/inv/data');
            } else if ( files.rfile.name.length > 0 ) {
                res.redirect('/inv/resp');
            } else if ( files.mfile.name.length > 0 ) {
                res.redirect('/inv/model');
            }
        });        
    } catch {
    }
}

const inv_index_get = (req,res) => {
    
    res.render('./inv/index', {title: "welcome page", name: req.user.name,
    rho: "", xS: "", zS: "",
    cmin: "", cmax: "", clabel: 'Resistivity [Ohm m]',
    xmin: "", xmax: "", xlabel: 'Surface Distance [m]',
    zmin: "", zmax: "", zlabel: 'Elevation [m]', 
    title_plot: 'Inverted Model', 
    tick: "", tickval: ""});
}

const inv_login_get = (req,res) => {
    res.render('./inv/login', {title: "login page", err: ""});
}

const inv_register_post = (req,res) => {
    try{
        const pass1 = req.body.password;
        const pass2 = req.body.cpassword;
        // check length of password
        if (pass1.length < 8) {
            res.render('./inv/register', {title: "register page", err: "1"});
            return;
        }

        // check consistency of both password
        if ( ! (pass1.toString().trim() === pass2.toString().trim()) ) {
            res.render('./inv/register', {title: "register page", err: "2"});
            return;
        }
        
        // validating email
        mems.find({email: req.body.email})
        .then( result => {
            if ( result.length > 0 ){
                res.render('./inv/register', {title: "register page", err: "3"});
            } else {
                mems.find({username: req.body.username})
                .then( async result => {
                    if ( result.length > 0 ){
                        res.render('./inv/register', {title: "register page", err: "4"});    
                    } else {
                        const mem = new mems(req.body);
                        const salt = await bcrypt.genSalt()
                        const hashedPassword = await bcrypt.hash(mem.password, salt)
                        mem.password = hashedPassword;
                
                        mem.save()
                        .then( (result) => {
                            res.redirect('/inv/login');
                        }) 
                        .catch( (err) => {
                            console.log(err);
                        });
                    }
                });
            };
        });
    } catch {

    }
}

const inv_register_get = (req,res) => {
    res.render('./inv/register', {title: "register page", err: ""});
}

const inv_logout = (req,res) => {
    req.logout();
    res.redirect('/inv/login');
}


module.exports = {
    inv_index_post,
    inv_index_get,
    inv_login_post,
    inv_login_get,
    inv_register_post,
    inv_register_get,
    inv_logout,
    inv_model_get,
    inv_resp_get,
    inv_data_get,
    inv_model_post
}
