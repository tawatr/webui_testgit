function extractData(arg) {
    let { makeArray } = require('./makeArray');

    let NoData = arg.NoData;
    let rawdata = arg.rawdata;
    let ESpacing = arg.ESpacing;
    let array_type = arg.array_type;

    switch (array_type) {
        case 1: // wenner-alpha
            nelec = 4;
            col = [1, 3, 5, 7];
            apprho_index = 9;
        case 3: // dipole-dipole
            nelec = 4;
            col = [1, 3, 5, 7];
            apprho_index = 9;
        case 7: // wenner-schlumberger
            nelec = 4;
            col = [1, 3, 5, 7];
            apprho_index = 9;
        default:
            nelec = 4;
            col = [1, 3, 5, 7];
            apprho_index = 9;
    }

    let kk = 0;
    elecx = makeArray(1,NoData*nelec,0)
    for (jj = 0; jj < NoData; jj++){
        for (ii = 0; ii < nelec; ii++) {
            elecx[kk] = rawdata[jj][ col[ii] ];
            kk = kk + 1;
        }
    }

    elecx = elecx.filter( (value, indelecx, array) => {
        return array.indexOf(value) === indelecx;
    }).sort( (a,b) => {return a-b} );
    
    let noelec = elecx.length;

    let Electrode = makeArray(nelec, NoData, 0);
    let apprho = makeArray(1, NoData, 0);
    let xres = makeArray(1, NoData, 0);
    let zres = makeArray(1, NoData, 0);
    
    for (ii = 0; ii < NoData; ii++) {
        for (jj = 0; jj < nelec; jj++){
            Electrode[ii][jj] = elecx.indexOf(rawdata[ii][col[jj]]) + 1;
        }
        apprho[ii] = Math.log10(rawdata[ii][apprho_index]);
        min_index = Math.min.apply(Math, Electrode[ii]);
        max_index = Math.max.apply(Math, Electrode[ii]);
        xres[ii] = (max_index + min_index)*ESpacing/2.0;
        zres[ii] = (max_index - min_index)*ESpacing/4;
    }
    
    let pelec = makeArray(2, noelec, undefined);
    let cindex = 0;
    let rindex = 0;
    for (ii = 0; ii < noelec; ii++){
        pelec[ii][0] = elecx[ii];
        if (pelec[ii][1] == undefined ){
            cindex = undefined;
            rindex = undefined;
            for (jj = 0; jj < NoData; jj++){
                for (kk = 0; kk < nelec; kk++) {
                    if ( Electrode[jj][kk] - ii == 1 ){
                        cindex = col[kk];
                        rindex = jj;
                        break;
                    }
                }
                if (cindex != undefined) {
                    break;
                }
            }
            pelec[ii][1] = rawdata[rindex][cindex+1];
        }
    }
    
    return {
        apprho,
        Electrode,
        noelec,
        pelec,
        xres,
        zres
    }
}

module.exports = {extractData: extractData};