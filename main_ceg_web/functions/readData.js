async function read_dat_datafile(dfile) {
    const fs = require('fs');
    const readline = require('readline');
    let { makeArray } = require('./makeArray');
    let Profile_Name = '';
    let ESpacing  = 0;
    let format_type = 0;
    let array_type = 0;
    let NoData = 0;
    let data = [];
    let num_d = 0;
    let num_a = 0;
    let rawdata = [];
    let ext = "dat";
    
    const fileStream = fs.createReadStream(dfile);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    

    let ii = 1;     
    for await (const line of rl) {
        if (ii == 1) {
            Profile_Name = line;
        } else if (ii == 2) {
            ESpacing = parseFloat(line);
        } else if (ii == 3) {
            format_type = parseInt(line);
        } else if (ii == 4) {
            array_type = parseInt(line);
        } else if (ii == 7) {
            let f = line;
            NoData = parseInt(f);
        } else if (ii == 9) {
            let h = line;
            num_d = 10;
            num_all = num_d + NoData;
            num_a = parseInt(num_all);
            rawdata = makeArray(num_d, NoData , 0);
            data = makeArray(num_d, NoData , 0);
        } else if (ii >= num_d && ii <= num_a - 1 ){
            data[ii - num_d] = line.split(/[ \s/]+/);

            for (jj = 0; jj < num_d; jj++) {
                rawdata[ii - num_d][jj] = parseFloat(data[ii - num_d][jj]);
            }
        }
        ii++;
    }

    return {
        Profile_Name,
        ESpacing,
        array_type,
        NoData,
        rawdata,
        ext
    };
} 
module.exports = {readData: read_dat_datafile};