async function read_m2d_modelfile(mfile) {
    const fs = require('fs');
    const readline = require('readline');
    let noelX = 0;
    let noelZ = 0;
    let xS = [];
    let zS = [];
    let rho = [];
    let initx = 0;
    let initz = 0;
    let initr = 0;
    
    const fileStream = fs.createReadStream(mfile);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    
    let ii = 1;     
    for await (const line of rl) {
        if (ii == 1) {
            const index = line.search(':');
        } else if (ii == 2) {
            [noelX, noelZ] = line.split(" ");
            noelX = parseInt(noelX);
            noelZ = parseInt(noelZ);
            initx = 4;
            initz = initx + noelX + 2;
            initr = initz + noelZ + 2;
        } else if ( ii >= initx && ii < initz-1 ) {
            xS[ii - initx] = parseFloat(line);
        } else if ( ii >= initz && ii < initr-1) {
            zS[ii- initz] = parseFloat(line);
        } else if ( ii >= initr) {
            rho[ii- initr] = parseFloat(line);
        }
        ii++;
    }

    return {
        xS,
        zS,
        noelX,
        noelZ,
        rho
    };
};

module.exports = {readModel: read_m2d_modelfile};