function plotOption_data(arg){
    r = arg.r;

    // set min and max of colorbar
    let apprho = r.apprho;
    if ( arg.cmin == undefined ) {
        cmin = Math.min.apply(Math, apprho);
    } else {
        cmin = Math.log10(arg.cmin);
    }
    
    if ( arg.cmax == undefined ) {
        cmax = Math.max.apply(Math, apprho);
    } else {
        cmax = Math.log10(arg.cmax);
    }
    
    // get left boundary
    if ( arg.xmin == undefined ) {
        xmin = Math.min.apply(Math, r.xres);
    } else {
        xmin = arg.xmin;
    }

    // get right boundary
    if ( arg.xmax == undefined ) {
        xmax = Math.max.apply(Math, r.xres);
    } else {
        xmax = arg.xmax;
    }

    // get top boundary
    if ( arg.zmin == undefined ) {
        zmin = Math.min.apply(Math, r.zres);
    } else {
        zmin = arg.zmin;
    }

    // get bottom boundary
    if ( arg.zmax == undefined ) {
        zmax = Math.max.apply(Math, r.zres);
    } else {
        zmax = arg.zmax;
    }

    // get xlabel
    if ( arg.xlabel == undefined ) {
        xlabel = 'Horizontal Distance [m]';
    } else {
        xlabel = arg.xlabel;
    }

    // get zlabel
    if ( arg.zlabel == undefined ) {
        zlabel = 'Pseudo Depth [m]';
    } else {
        zlabel = arg.zlabel;
    }

    // get title
    if ( arg.title == undefined ) {
        title = 'Model Response';
    } else {
        title = arg.title;
    }

    // get clabel
    if ( arg.clabel == undefined ) {
        clabel = 'Apparent Resistivity [Ohm m]';
    } else {
        clabel = arg.clabel;
    }

    let tickval =[];
    let tick =[];
    let ncolor = 10;
    let step = (cmax-cmin)/ncolor;
    tickval[0] = cmin;
    tick[0] = Math.round(Math.pow(10,tickval[0]));
    for ( ii = 1; ii < ncolor+1; ii++) {
        tickval[ii] = tickval[ii-1] + step;
        tick[ii] = Math.round(Math.pow(10,tickval[ii]));
    }
    
    return {
        cmin,
        cmax,
        clabel,
        xmin,
        xmax,
        xlabel,
        zmin,
        zmax,
        zlabel,
        title,
        tickval,
        tick    
    };
};

module.exports = {plotOption_data: plotOption_data};