function extractModel(arg) {
    let { makeArray } = require('./makeArray');
    var noelX = arg.noelX;
    var noelZ = arg.noelZ;
    var xS = arg.xS;
    var zS = arg.zS;
    var rho = arg.rho;

    let xS_plot = makeArray(noelZ*noelX,0);
    let zS_plot = makeArray(noelZ*noelX,0);
    let kk = 0;
    
    for (jj = 0; jj < noelX; jj++){
        for (ii = 0; ii < noelZ; ii++){
            xS_plot[kk] = (xS[jj] + xS[jj+1])/2;
            zS_plot[kk] = (zS[ii] + zS[ii+1])/2;
            rho[kk] = Math.log10(rho[kk]);
            if ( rho[kk] > 6) {
                rho[kk] = NaN;
            }
            kk = kk + 1;
        }
    }

    return {
        xS_plot,
        zS_plot,
        rho
    };
}

module.exports = {extractModel: extractModel};