function plotOption_model(arg){
    m = arg.m;

    // set min and max of colorbar
    let rho = m.rho.filter( p => !isNaN(p) ) ;
    if ( arg.cmin == undefined ) {
        cmin = Math.min.apply(Math, rho);
    } else {
        cmin = Math.log10(arg.cmin);
    }
    
    if ( arg.cmax == undefined ) {
        cmax = Math.max.apply(Math, rho);
    } else {
        cmax = Math.log10(arg.cmax);
    }
    
    if ( cmax == cmin ) {
        cmin = cmin-1;
        cmax = cmax+1;
    }
    // get left boundary
    if ( arg.xmin == undefined ) {
        console.log('left boundary is undefined');
        return;
    } else {
        xmin = arg.xmin;
    }

    // get right boundary
    if ( arg.xmax == undefined ) {
        console.log('right boundary is undefined');
        return;
    } else {
        xmax = arg.xmax;
    }

    // get top boundary
    if ( arg.zmin == undefined ) {
        console.log('top boundary is undefined');
        return;
    } else {
        zmin = arg.zmin;
    }

    // get bottom boundary
    if ( arg.zmax == undefined ) {
        console.log('bottom boundary is undefined');
        return;
    } else {
        zmax = arg.zmax;
    }

    // get xlabel
    if ( arg.xlabel == undefined ) {
        xlabel = 'Horizontal Distance [m]';
    } else {
        xlabel = arg.xlabel;
    }

    // get zlabel
    if ( arg.zlabel == undefined ) {
        zlabel = 'Depth [m]';
    } else {
        zlabel = arg.zlabel;
    }

    // get title
    if ( arg.title == undefined ) {
        title = 'Model';
    } else {
        title = arg.title;
    }

    // get clabel
    if ( arg.clabel == undefined ) {
        clabel = 'Resistivity [Ohm m]';
    } else {
        clabel = arg.clabel;
    }

    let tickval =[];
    let tick =[];
    let ncolor = 10;
    let step = (cmax-cmin)/ncolor;
    tickval[0] = cmin;
    tick[0] = Math.round(Math.pow(10,tickval[0]));
    for ( ii = 1; ii < ncolor+1; ii++) {
        tickval[ii] = tickval[ii-1] + step;
        tick[ii] = Math.round(Math.pow(10,tickval[ii]));
    }
    
    return {
        cmin,
        cmax,
        clabel,
        xmin,
        xmax,
        xlabel,
        zmin,
        zmax,
        zlabel,
        title,
        tickval,
        tick    
    };
};

module.exports = {plotOption_model: plotOption_model};