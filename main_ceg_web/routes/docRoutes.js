const express = require('express');
const router_doc = express.Router();
const docController = require('../controllers/docController');

router_doc.get('/index', docController.doc_index_get);
router_doc.get('/receipt', docController.doc_receipt_get);
router_doc.post('/receipt', docController.doc_receipt_post);

module.exports = router_doc;
