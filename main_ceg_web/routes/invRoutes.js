const express = require('express');
const router = express.Router();
const invController = require('../controllers/invController');

router.get('/model', checkAuthenticated, invController.inv_model_get);
router.post('/model', checkAuthenticated, invController.inv_model_post);
router.get('/resp', checkAuthenticated, invController.inv_resp_get);
router.get('/data', checkAuthenticated, invController.inv_data_get);
router.post('/index', checkAuthenticated, invController.inv_index_post);
router.get('/index', checkAuthenticated, invController.inv_index_get);
router.post('/login', invController.inv_login_post);
router.get('/login', checkNoAuthenticated, invController.inv_login_get);
router.get('/register', checkNoAuthenticated, invController.inv_register_get);
router.post('/register', invController.inv_register_post);
router.get('/logout', invController.inv_logout);

module.exports = router;

function checkAuthenticated(req,res, next) {
    if( req.isAuthenticated()) {
        return next();
    } 

    res.redirect('/inv/login')
}

function checkNoAuthenticated(req,res, next) {
    if( req.isAuthenticated()) {
        return res.redirect('/inv/index')
    } 

    next();
}