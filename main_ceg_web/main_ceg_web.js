// Test message after checkout -
const morgan = require('morgan');
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const flash = require("express-flash") // used inside passport for display massage
const session = require("express-session") // persist data across difference page
const invRoutes = require('./routes/invRoutes');
const docRoutes = require('./routes/docRoutes');
const passport = require('passport');
require("./passport-config")(passport);
const mems = require('./models/mem_account');


require("dotenv").config();

const dbURI = 'mongodb://' + process.env.DB_LOCAL_HOST + '/';
mongoose.connect(dbURI, {user: process.env.DB_ROOT, pass: process.env.DB_PASSWORD, dbName: process.env.DB_DATABSE, useNewUrlParser: true, useUnifiedTopology: true})
.then((result) => app.listen(3000))
.catch((err) => console.log(err));

// register view engine
app.set('view engine', 'ejs');
app.set('views', 'views');

// middleware
// Static designates folder public to clients
app.use(express.static('public'));
app.use(express.urlencoded( { extended: true} ) );
app.use(morgan('dev'));
// Protect double login
app.use(session( {
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true.valueOf,
    rfile: '',
    mfile: '',
    dfile: ''
} ));

// passport is for login
app.use(passport.initialize());
app.use(passport.session());
// flast() quick report message.
app.use(flash());

// calling router for doc program
app.use('/doc', docRoutes);

// calling router for inversion program
app.use('/inv', invRoutes);


app.use( (req, res) => {
    //    res.sendFile('./views/404.html', {root: __dirname });
    res.status(404).render('./404', { title: '404'});
});
